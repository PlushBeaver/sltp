﻿#pragma once

#ifndef __AFXWIN_H__
	#error "Нужно включить stdafx.h в PCH до включения данного файла."
#endif

// 
///	Класс приложения-клиента String List Transfer Protocol (SLTP). 
///	Отвечает за начальную инициализацию всех подсистем и показ главного окна.
///	
class CClientApp : public CWinApp
{
public:
	virtual BOOL InitInstance();
};

// Глобальный объект приложения-клиента.
extern CClientApp theApp;