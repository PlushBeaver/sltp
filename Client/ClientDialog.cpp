﻿#include "stdafx.h"

#include "Client.h"
#include "ClientDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CClientDialog::CClientDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CLIENT_DIALOG, pParent), m_socket(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_state = CClientDialog::Initializing;
}

void CClientDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDIT_SERVER_HOST, serverHostEdit);
	DDX_Control(pDX, IDC_EDIT_SERVER_PORT, serverPortEdit);
	DDX_Control(pDX, IDC_EDIT_NEW_STRING, newStringEdit);
	DDX_Control(pDX, IDC_BUTTON_TOGGLE_CONNECTION, toggleConnectionButton);
	DDX_Control(pDX, IDC_BUTTON_ADD_STRING, addStringButton);
	DDX_Control(pDX, IDC_LISTBOX_STRINGS, stringsListBox);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_STRING, removeStringButton);
	DDX_Control(pDX, IDC_SPIN_PORT, portSpinButton);

	switch (m_state)
	{
	case ChoosingServer:
		DDX_Text(pDX, IDC_EDIT_SERVER_HOST, m_serverHost);
		if (m_serverHost.Trim().IsEmpty())
		{
			AfxMessageBox(_T("Адрес сервера не должен быть пуст!"), MB_OK | MB_ICONWARNING);
			pDX->Fail();
		}
		DDX_Text(pDX, IDC_EDIT_SERVER_PORT, m_serverPort);
		DDV_MinMaxUInt(pDX, m_serverPort, 1, 65535);
		break;

	case ExchangingWithStrings:
		DDX_Text(pDX, IDC_EDIT_NEW_STRING, m_newString);
		DDX_LBIndex(pDX, IDC_LISTBOX_STRINGS, m_selectedStringIndex);
		break;

	case Initializing:
		m_state = ChoosingServer;
		break;
	}
}

BEGIN_MESSAGE_MAP(CClientDialog, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_TOGGLE_CONNECTION, &CClientDialog::OnBnClickedButtonToggleConnection)
	ON_BN_CLICKED(IDC_BUTTON_ADD_STRING, &CClientDialog::OnBnClickedButtonAddString)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_STRING, &CClientDialog::OnBnClickedButtonRemoveString)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


BOOL CClientDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	serverHostEdit.SetWindowText(_T("localhost"));
	portSpinButton.SetBuddy(CWnd::FromHandle(serverPortEdit.m_hWnd));
	portSpinButton.SetRange32(1, 65535);
	portSpinButton.SetPos32(12345);

	return TRUE;  // Возврат TRUE означает, что фокус не передан элементу управления.
}


void CClientDialog::OnRemoveString(const SLTP::STRING_ID stringID)
{
	for (int i = 0; i < stringsListBox.GetCount(); i++)
	{
		if (stringsListBox.GetItemData(i) == stringID)
		{
			stringsListBox.DeleteString(i);
			break;
		}
	}
}


void CClientDialog::OnClearList()
{
	stringsListBox.ResetContent();
}


void CClientDialog::OnAcceptString(const SLTP::STRING_ID stringID, const CString& string)
{
	int index = stringsListBox.AddString(string);
	stringsListBox.SetItemData(index, stringID);
}


void CClientDialog::OnBnClickedButtonToggleConnection()
{
	if (m_socket)
	{
		m_socket->Close();
		OnDisconnected();
	}
	else
	{
		if (UpdateData())
		{
			SetConnectionControlsEnabled(false);
			toggleConnectionButton.EnableWindow(FALSE);
			m_socket.Attach(new CSltpClientSocket(*this));
			m_socket->Create();
			m_socket->Connect(m_serverHost, m_serverPort);
		}
	}
}


void CClientDialog::OnBnClickedButtonAddString()
{
	UpdateData();
	newStringEdit.SetWindowText(L"");
	m_socket->AddString(m_newString.GetString());
}


void CClientDialog::OnBnClickedButtonRemoveString()
{
	UpdateData();
	if (m_selectedStringIndex >= 0)
	{
		DWORD_PTR stringID = stringsListBox.GetItemData(m_selectedStringIndex);
		stringsListBox.DeleteString(m_selectedStringIndex);
		m_socket->RemoveString(stringID);
	}
}


void CClientDialog::OnConnected()
{
	m_state = ExchangingWithStrings;
	SetStringListControlsEnabled(true);
	toggleConnectionButton.EnableWindow(TRUE);
	toggleConnectionButton.SetWindowText(_T("Отключиться"));
}


void CClientDialog::OnDisconnected()
{
	m_state = ChoosingServer;
	SetStringListControlsEnabled(false);
	SetConnectionControlsEnabled(true);
	toggleConnectionButton.EnableWindow(TRUE);
	toggleConnectionButton.SetWindowText(_T("Подключиться"));
	m_socket.Free();
}


void CClientDialog::SetStringListControlsEnabled(const bool enabled)
{
	newStringEdit.EnableWindow(enabled);
	addStringButton.EnableWindow(enabled);
	stringsListBox.EnableWindow(enabled);
	removeStringButton.EnableWindow(enabled);
}

void CClientDialog::SetConnectionControlsEnabled(const bool enabled)
{
	serverHostEdit.EnableWindow(enabled);
	serverPortEdit.EnableWindow(enabled);
	portSpinButton.EnableWindow(enabled);
}


void CClientDialog::OnErrorOccurred(const CString& errorMessage)
{
	OnDisconnected();
	AfxMessageBox(errorMessage, MB_OK | MB_ICONERROR);
}


void CClientDialog::OnOK()
{
	if (m_socket)
	{
		if (GetFocus()->GetDlgCtrlID() == newStringEdit.GetDlgCtrlID())
		{
			OnBnClickedButtonAddString();
		}
	}
	else
	{
		OnBnClickedButtonToggleConnection();
	}
}


void CClientDialog::OnClose()
{
	CDialogEx::OnCancel();
}
