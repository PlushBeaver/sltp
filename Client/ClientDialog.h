﻿#pragma once

#include <afxwin.h>

#include "resource.h"

#include "../Library/SLTP.h"
#include "SltpClientSocket.h"

 
// Главное окно приложения-клиента в форме диалога Windows.
// Содержит сокет-клиент SLTP, для которого является слушателем событий.
class CClientDialog : public CDialogEx, public SLTP::IClientSideEventListener
{
public:
	CClientDialog(CWnd* pParent = NULL);


protected:
	// Реализует поддержку Dialog Data Exchange & Validation (DDX/DDV) ---
	// органичного способа передачи данных из элементов управления в программу
	// с проверкой вводимых пользователем значений.
	virtual void DoDataExchange(CDataExchange* pDX);


public:	// Реализация интерфейса ISltpClientSideEventListener.

	// Вызывается по получении команды удалить строку из списка, что и делает.
	virtual void OnRemoveString(const SLTP::STRING_ID stringID);

	// Вызывается по получении команды очистить список строк, что и делает.
	virtual void OnClearList();

	// Вызывается по приходе сообщения об утверждении строки в списке
	// с выданнымидентификатором. Добавляет строку в список.
	virtual void OnAcceptString(const SLTP::STRING_ID stringID, const CString& string);

	// Вызывается после успешного соединения. Изменяет доступность элементов управления.
	virtual void OnConnected();

	// Вызывается после разрыва соединения. Изменяет доступность элементов управления.
	virtual void OnDisconnected();

	// вызывается при возникновении ошибки. Вызывает разрыв соединения.
	virtual void OnErrorOccurred(const CString& errorMessage);


private: // Детали реализации.

	// Устанавливает доступность элементов управления списком строк.
	void SetStringListControlsEnabled(const bool enabled);

	// Устанавливает доступность элементов управления списком строк.
	void SetConnectionControlsEnabled(const bool enabled);


private:
	// Сокет-клиент SLTP.
	CAutoPtr<CSltpClientSocket> m_socket;

	// Состояние диалога: инициализация, ввод адреса сервера, обмен строками.
	// Используется Dialog Data Exchange & Validation (DDX/DDV).
	enum { Initializing, ChoosingServer, ExchangingWithStrings } m_state;

	// Хост адреса сервера.
	CString m_serverHost;

	// Порт адреса сервера.
	UINT m_serverPort;

	// Введенная строка для добавления.
	CString m_newString;

	// Индекс выбранной в списке строки.
	int m_selectedStringIndex;


	
protected: // Детали реализации, связанные с MFC.

	// Значок окна.
	HICON m_hIcon;

	// Вызывается при инициализации диалога. Настраивает элементы управления.
	virtual BOOL OnInitDialog();

	// Карта сообщений (используется MFC для реакции на события).
	DECLARE_MESSAGE_MAP()

	
public: // Элементы управления.

	CEdit serverHostEdit;
	CEdit serverPortEdit;
	CEdit newStringEdit;
	CButton toggleConnectionButton;
	CButton addStringButton;
	CListBox stringsListBox;
	CButton removeStringButton;
	CSpinButtonCtrl portSpinButton;

	
private: // Обработчики событий элементов управления.
	afx_msg void OnBnClickedButtonToggleConnection();
	afx_msg void OnBnClickedButtonAddString();
	afx_msg void OnBnClickedButtonRemoveString();

public: // Переопределенные методы диалога.

	// Вызывается при окончании диалога положительным результатом (кнопка "ОК" или ВВОД).
	// Переопределен, чтобы запускать сервер, если он не запущен, и окно не закрывать.
	virtual void OnOK();

	// Вызывается при окончании диалога отрицательным результатом (кнопка "Отмена" или Escape).
	// Переопределен, чтобы ничего не делать и окно не закрывать.
	virtual void OnCancel() { }

	// Вызывается при закрытии окна "крестиком" или из панели задач.
	// Перекрыт, чтобы игнорирование окончания работы с диалогом (OnCancel)
	// не мешало нормальному закрытию окна.
	afx_msg void OnClose();
};
