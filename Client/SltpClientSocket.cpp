﻿#include "stdafx.h"
#include "SltpClientSocket.h"


CSltpClientSocket::CSltpClientSocket(SLTP::IClientSideEventListener& eventListener) 
	: m_eventListener(eventListener)
{
}

void CSltpClientSocket::OnConnect(int nErrorCode)
{
	CSltpSocket::OnConnect(nErrorCode);
	if (!nErrorCode)
	{
		m_eventListener.OnConnected();
	}
}

void CSltpClientSocket::OnClose(int nErrorCode)
{
	CSltpSocket::OnClose(nErrorCode);
	m_eventListener.OnDisconnected();
}

CSltpClientSocket::ParsingState CSltpClientSocket::OnCommandParsed()
{
	switch (m_command)
	{
	case CommandCode_ClearList:
		m_eventListener.OnClearList();
		return ParsingState_Command;
	}
	return CSltpSocket::OnCommandParsed();
}

CSltpClientSocket::ParsingState CSltpClientSocket::OnStringDataParsed(const CString& string)
{
	switch (m_command)
	{
	case CommandCode_AcceptString:
		m_eventListener.OnAcceptString(m_stringID, string);
		return ParsingState_Command;
	}
	return ParsingState_Invalid;
}

void CSltpClientSocket::AddString(const CString& string)
{
	SendByte(CommandCode_AddString);
	SendString(string);
	OnSend(0);
}

void CSltpClientSocket::RemoveString(const SLTP::STRING_ID stringID)
{
	SendByte(CommandCode_RemoveString);
	SendInteger(stringID);
	OnSend(0);
}
