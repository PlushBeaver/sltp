﻿#pragma once

#include "../Library/SLTP.h"
#include "../Library/SltpSocket.h"

// Сокет клиентской стороны SLTP.
class CSltpClientSocket : public CSltpSocket, public SLTP::IClientSide
{
public:
	// Создает сокет клиентской стороны SLTP, оповещающий о событиях указанного слушателя.
	CSltpClientSocket(SLTP::IClientSideEventListener& eventListener);
	

protected:	// Переопределенные методы сокета.

	// Вызывается после установки соединения с сервером, о чем оповещает слушателя событий.
	virtual void OnConnect(int nErrorCode);

	// Вызывается после закрытия соединения с сервером, о чем оповещает слушателя событий.
	virtual void OnClose(int nErrorCode);


public:	// Реализация интерфейса SLTP::IClientSide.

	// Посылает серверу запрос на добавление строки в список.
	virtual void AddString(const CString& string);

	// Посылает серверу команду удалить строку из списка по идентификатору.
	virtual void RemoveString(const SLTP::STRING_ID stringID);


protected:	// Переопределение и реализация методов поддержки SLTP.

	// Вызывается после разбора кода команды. Для клиента обрабатывается
	// очистка списка и удаление строки, о чем оповещается слушатель событий.
	virtual ParsingState OnCommandParsed();

	// Вызывается после считывания данных строки. Для клиента это команда 
	// утверждения строки в списке, о чем оповещается слушатель событий.
	virtual ParsingState OnStringDataParsed(const CString& string);

	// Возвращает интерфейс слушателя событий любой стороны протокола.
	virtual SLTP::IEventListener& GetEventListener() const { return m_eventListener; }


private:
	// Слушатель событий клиентской стороны.
	SLTP::IClientSideEventListener& m_eventListener;
};

