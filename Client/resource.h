//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Client.rc
//
#define IDD_CLIENT_DIALOG               102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDS_CONNECT                     104
#define IDS_DISCONNECT                  105
#define IDR_MAINFRAME                   128
#define IDC_EDIT_NEW_STRING             1000
#define IDC_EDIT_SERVER_PORT            1001
#define IDC_SPIN_PORT                   1002
#define IDC_BUTTON_TOGGLE_CONNECTION    1003
#define IDC_LISTBOX_STRINGS             1004
#define IDC_BUTTON_ADD_STRING           1005
#define IDC_EDIT_SERVER_HOST            1006
#define IDC_BUTTON_REMOVE_STRING        1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
