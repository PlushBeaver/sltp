﻿#include "stdafx.h"
#include "BufferedAsyncSocket.h"

CBufferedAsyncSocket::CBufferedAsyncSocket() : CAsyncSocket()
{
	m_inputPosition = 0;
}

void CBufferedAsyncSocket::OnConnect(int nErrorCode)
{
	if (!nErrorCode)
	{
		m_inputPosition = 0;
		m_input.SetSize(0);
		m_output.SetSize(0);
		CAsyncSocket::OnConnect(nErrorCode);
	}
	else
	{
		RaiseSocketError(nErrorCode);
	}
}

void CBufferedAsyncSocket::OnClose(int nErrorCode)
{
	m_inputPosition = 0;
	m_input.SetSize(0);
	m_output.SetSize(0);
	CAsyncSocket::OnClose(nErrorCode);
}

void CBufferedAsyncSocket::OnReceive(int nErrorCode)
{
	if (nErrorCode)
	{
		RaiseSocketError(nErrorCode);
		return;
	}
	DWORD bytesAvailable = 0;
	IOCtl(FIONREAD, &bytesAvailable);
	if (bytesAvailable > 0)
	{
		m_input.RemoveAt(0, m_inputPosition);
		m_input.SetSize(m_input.GetCount() + bytesAvailable);
		int bytesReceived = Receive(m_input.GetData(), bytesAvailable);
		if (bytesReceived < 0 || static_cast<DWORD>(bytesReceived) != bytesAvailable)
		{
			RaiseSocketError(this->GetLastError());
			return;
		}
		m_inputPosition = 0;
	}
	CAsyncSocket::OnReceive(nErrorCode);
}


void CBufferedAsyncSocket::OnSend(int nErrorCode)
{
	INT_PTR bytesSendThisTime = 0;
	while (bytesSendThisTime < m_output.GetCount())
	{
		int bytesSent = Send(m_output.GetData() + bytesSendThisTime, m_output.GetCount() - bytesSendThisTime);
		if (bytesSent == SOCKET_ERROR)
		{
			int errorCode = GetLastError();
			if (errorCode != WSAEWOULDBLOCK)
			{
				RaiseSocketError(errorCode);
			}
			break;
		}
		else
		{
			bytesSendThisTime += bytesSent;
		}
	}
	m_output.RemoveAt(0, bytesSendThisTime);
	CAsyncSocket::OnSend(nErrorCode);
}

const bool CBufferedAsyncSocket::HasReceived(const size_t byteAmount) const
{
	return m_input.GetCount() - m_inputPosition >= byteAmount;
}

const bool CBufferedAsyncSocket::TryReceiveData(BYTE *data, const size_t amount)
{
	if (HasReceived(amount))
	{
		memcpy(data, m_input.GetData() + m_inputPosition, amount);
		m_inputPosition += amount;
		return true;
	}
	return false;
}

const bool CBufferedAsyncSocket::TryReceiveByte(BYTE& result)
{
	if (HasReceived(sizeof BYTE))
	{
		result = m_input[m_inputPosition++];
		return true;
	}
	return false;
}

const bool CBufferedAsyncSocket::TryReceiveInteger(DWORD& result)
{
	return TryReceiveData(reinterpret_cast<BYTE*>(&result), sizeof DWORD);
}

void CBufferedAsyncSocket::SendData(const BYTE *buffer, const size_t size)
{
	// KILLME: Это такой дурацкий официальный способ помещать данные в буфер.
	INT_PTR oldCount = m_output.GetCount();
	m_output.SetSize(oldCount + size);
	for (size_t i = 0; i < size; i++)
	{
		INT_PTR index = oldCount + i;
		m_output.SetAt(index, buffer[i]);
	}
}

void CBufferedAsyncSocket::SendByte(const BYTE byte)
{
	SendData(&byte, sizeof byte);
}

void CBufferedAsyncSocket::SendInteger(const DWORD integer)
{
	SendData(reinterpret_cast<const BYTE*>(&integer), sizeof integer);
}