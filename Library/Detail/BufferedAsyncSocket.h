﻿#pragma once

#include <afxsock.h>

// Асинхронный сокет с буферизацией данных.
class CBufferedAsyncSocket : public CAsyncSocket
{
public:
	CBufferedAsyncSocket();

	
protected: // Переопределенные функции CAsyncSocket.

	// Вызывается при установлении соединения. Информирует наследников об ошибках. 
	virtual void OnConnect(int nErrorCode);

	// Вызывается при возможности принять данные. 
	// Накапливает принятые данные во внутреннем буфере.
	virtual void OnReceive(int nErrorCode);

	// Вызывается при возможности отправить данные. Отправляет данные
	// из внутреннего буфера, сколько возможно, и освобождает часть буфера.
	virtual void OnSend(int nErrorCode);

	// Вызывается при окончании сеанса работы. Освобождает память под буферы.
	virtual void OnClose(int nErrorCode);


public: // Вспомогательные методы для передачи данных.

	// Проверяет, что получено указанное число байт.
	const bool HasReceived(const size_t byteAmount) const;

	///	Читает данные длиной amount байт из внутренного буфера в data.
	///	Возвращает false, если в буфере недостаточно данных, иначе true.
	const bool TryReceiveData(BYTE *data, const size_t amount);

	// Версия <see cref="TryReceiveData"/> для одного байта.
	const bool TryReceiveByte(BYTE& result);

	// Версия <see cref="TryReceiveData"/> для целого числа.
	const bool TryReceiveInteger(DWORD& result);

	///	Помещает во внутренний буфер для последующей отправки 
	///	данные buffer размером size байт.
	void SendData(const BYTE *buffer, const size_t size);

	// Помещает во внутренний буфер для отправки 1 байт.
	void SendByte(const BYTE byte);

	// Помещает во внутренний буфер для отправки целое число.
	void SendInteger(const DWORD integer);


protected: // Методы для реализации в наследниках.

	// Оповещает слушателя событий об ошибке сокета, сопоставляя коду сообщение.
	void virtual RaiseSocketError(int errorCode) = 0;


private:
	// Внутренний буфер принятых данных.
	CByteArray m_input;

	// Текущая позиция считывания принятых данных из буфера.
	size_t m_inputPosition;

	// Внутренний буфер данных для пересылки.
	CByteArray m_output;
};

