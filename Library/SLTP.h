﻿#ifndef STRING_LIST_TRANSFER_PROTOCOL_H
#define STRING_LIST_TRANSFER_PROTOCOL_H

#include <afx.h>	// Для CString и DWORD.

// Содержит логические сущности String List Transfer Protocol (SLTP).
namespace SLTP
{
	// Порт по умолчанию для протокола.
	const unsigned short DefaultPort = 12345;


	// Идентификатор строки.
	typedef DWORD STRING_ID;


	// Серверная сторона.
	class IServerSide
	{
	public:
		// Утверждает строку в списке с указанным идентификатором.
		virtual void AcceptString(const CString& string, STRING_ID stringID) = 0;
		
		// Удаляет строку из списка по идентификатору.
		virtual void RemoveString(const STRING_ID stringID) = 0;

		// Очищает список.
		virtual void ClearList() = 0;
	};


	// Клиентская сторона.
	class IClientSide
	{
	public:
		// Предлагает строку для помещения в список.
		virtual void AddString(const CString& string) = 0;

		// Удаляет строку из списка по идентификатору.
		virtual void RemoveString(const STRING_ID stringID) = 0;
	};


	// Слушатель базового набора событий.
	class IEventListener
	{
	public:
		// Вызывается при удалении строки.
		virtual void OnRemoveString(const STRING_ID stringID) = 0;

		// Вызывается при возникновении ошибки.
		virtual void OnErrorOccurred(const CString& errorMessage) = 0;
	};


	// Слушатель событий клиентской стороны.
	class IClientSideEventListener : public IEventListener
	{
	public:
		// Вызывается при успешном подключении.
		virtual void OnConnected() = 0;

		// Вызывается при обрыве связи.
		virtual void OnDisconnected() = 0;

		// Вызывается при очистке списка.
		virtual void OnClearList() = 0;

		// ызывается при принятии строки сервером и присвоении ей идентификатора.
		virtual void OnAcceptString(const STRING_ID stringID, const CString& string) = 0;
	};


	// Слушатель событий серверной стороны.
	class IServerSideEventListener : public IEventListener
	{
	public:
		///	Вызывается при запросе клиента на добавление строки в список
		///	и присвоения ей сервером уникального идентификатора.
		virtual void OnAddString(const CString& string) = 0;

		// Вызывается при отключении передаваемого клиента.
		virtual void OnClientDisconnected(IServerSide& client) = 0;
	};
}

#endif