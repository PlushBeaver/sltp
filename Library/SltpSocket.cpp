﻿#include "stdafx.h"
#include "SltpSocket.h"

CSltpSocket::CSltpSocket(): CBufferedAsyncSocket()
{
	m_parsingState = ParsingState_Command;
}

void CSltpSocket::RaiseSocketError(int errorCode)
{
	CBufferedAsyncSocket::OnClose(errorCode);
	LPTSTR messageData;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
		0, errorCode, 0, (LPTSTR)&messageData, 0, 0);
	if (messageData)
	{
		GetEventListener().OnErrorOccurred(messageData);
		LocalFree(messageData);
	}
}



void CSltpSocket::OnReceive(int nErrorCode)
{
	// Должно быть в начале, так как базовый класс читает из сокета.
	CBufferedAsyncSocket::OnReceive(nErrorCode);

	bool canParse = true;
	do
	{
		canParse = false;
		switch (m_parsingState)
		{
		case ParsingState_Command:
			if (TryReceiveByte(m_command))
			{
				m_parsingState = OnCommandParsed();
				canParse = true;
			}
			break;
		case ParsingState_StringID:
			if (TryReceiveInteger(m_stringID))
			{
				m_parsingState = OnStringIDParsed();
				canParse = true;
			}
			break;
		case ParsingState_StringLength:
			if (TryReceiveInteger(m_stringLength))
			{
				m_parsingState = OnStringLengthParsed();
				canParse = true;
			}
			break;
		case ParsingState_StringData:
			if (CanParseString(m_stringLength))
			{
				TCHAR *string = new TCHAR[m_stringLength + 1];
				ParseString(string, m_stringLength);
				m_parsingState = OnStringDataParsed(string);
				delete[] string;
				canParse = true;
			}
			break;
		case ParsingState_Invalid:
		default:
			GetEventListener().OnErrorOccurred(_T("Ошибка протокола SLTP!"));
			break;
		}
	} 
	while (canParse);
}


CSltpSocket::ParsingState CSltpSocket::OnCommandParsed() 
{
	switch (m_command)
	{
	case CommandCode_AcceptString:
	case CommandCode_RemoveString:
		return ParsingState_StringID;
	};
	return ParsingState_Invalid;
}

CSltpSocket::ParsingState CSltpSocket::OnStringIDParsed() 
{
	switch (m_command)
	{
	case CommandCode_AcceptString:
		return ParsingState_StringLength;
	case CommandCode_RemoveString:
		GetEventListener().OnRemoveString(m_stringID);
		return ParsingState_Command;
	}
	return ParsingState_Invalid;
}

CSltpSocket::ParsingState CSltpSocket::OnStringLengthParsed() 
{
	return ParsingState_StringData;
}

void CSltpSocket::SendString(const TCHAR *string)
{
	const DWORD length = _tcslen(string);
	SendInteger(length);
	SendData(reinterpret_cast<const BYTE*>(string), (length + 1) * sizeof TCHAR);
}

void CSltpSocket::ParseString(TCHAR *string, const size_t length)
{
	TryReceiveData(reinterpret_cast<BYTE*>(string), sizeof TCHAR * (length + 1));
}

const bool CSltpSocket::CanParseString(const size_t length)
{
	return HasReceived((length + 1) * sizeof TCHAR);
}