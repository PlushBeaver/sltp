﻿#pragma once

#include "SLTP.h"
#include "Detail/BufferedAsyncSocket.h"


/// Базовый абстрактный класс сетевого сокета, ведущего передачу данных
/// по протоколу передачи списков строк (String List Transfer Protocol, SLTP).
///	Содержит функции базовой (независимой от стороны) поддержки протокола,
///	а также некоторый вспомогательный функционал для наследников.
class CSltpSocket : public CBufferedAsyncSocket
{
public:
	// Создает объект, оповещающий о событиях заданного слушателя.
	CSltpSocket();


protected:	// Переопределенные методы сокета.

	// Вызывается при возможности принять данные. Выполняет разбор пакетов,
	// вызывая обработчики различных полей, (пере)определенные в наследниках.
	virtual void OnReceive(int nErrorCode);


protected:	// Поддержка механизма разбора пакетов SLTP конечным автоматом.

	// Состояние разбора пакета.
	enum ParsingState
	{
		// Запрещенное состояние.
		ParsingState_Invalid,
		// Ожидается команда.
		ParsingState_Command,
		// Ожидается идентификатор строки.
		ParsingState_StringID,
		// Ожидается длина строки.
		ParsingState_StringLength,
		// Ожидаются данные строки.
		ParsingState_StringData
	};

	// Коды команд протокола.
	enum CommandCode
	{
		// Добавить строку (запрос клиентом идентикиафтора).
		CommandCode_AddString	 = 1,
		// Принятие строки (доставка сервером идентификатора).
		CommandCode_AcceptString = 2, 
		// Удаление строки.
		CommandCode_RemoveString = 3,
		// Очистка списка.
		CommandCode_ClearList	 = 4
	};

protected:
	// Состояние разбора очередного входящего пакета.
	ParsingState m_parsingState;
	// Текущая разбираемая команда.
	BYTE m_command;	
	// Считанный идентификатор строки.
	SLTP::STRING_ID m_stringID;
	// Считанная длина строки.
	DWORD m_stringLength;


protected:

	// Вызывается после разбора кода команды. Возвращает следующее состояние разбора.
	virtual ParsingState OnCommandParsed();	

	// Вызывается после считывания идентификатора строки. Возвращает следующее состояние.
	virtual ParsingState OnStringIDParsed();

	// Вызывается после считывания длины строки. Возвращает следующее состояние разбора.
	virtual ParsingState OnStringLengthParsed();

	// Вызывается после получения строки. Возвращает следующее состояние разбора.
	virtual ParsingState OnStringDataParsed(const CString& string) = 0;


protected:
	// Возвращает слушателя событий любой стороны протокола в наследнике.
	virtual SLTP::IEventListener& GetEventListener() const = 0;


protected: // Вспомогательные методы для унификации и согласованности формата передачи строк.

	// Отправляет строку с предварением ее длиной.
	void SendString(const TCHAR *string);

	// Определяет, получена ли строка заданной длины в символах.
	const bool CanParseString(const size_t length);

	// Принимает строку заданной длины в символах.
	void ParseString(TCHAR *string, const size_t length);

	// Оповещает слушателя событий об ошибке, сопоставляя коду сообщение.
	void virtual RaiseSocketError(int errorCode);
};

