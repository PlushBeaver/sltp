﻿#include "stdafx.h"
#include "Server.h"
#include "ServerDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "resource.h"


// Глобальный объект приложения-сервера.
CServerApp theApp;


// Выполняет инициализацию объекта приложения-сервера.
BOOL CServerApp::InitInstance()
{
	// Инициализация Common Controls -- расширенного набора
	// элементов управления (в частности, Up-Down control).
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Инициализация сетевой подсистемы (Windows Sockets 2).
	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Активация визуального диспетчера "Классический Windows" для включения элементов управления MFC.
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Создание главного окна-диалога и показ его на экране.
	CServerDialog dialog;
	m_pMainWnd = &dialog;
	if (dialog.DoModal() < 0)
	{
		AfxMessageBox(_T("Не удалось создать главное окно приложения-сервера!"), MB_OK | MB_ICONERROR);
		return FALSE;
	}

	// Не нужно запускать оконную процедуру для приложения, а просто выйти.
	return FALSE;
}

