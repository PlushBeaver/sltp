﻿#include "stdafx.h"

#include "Server.h"
#include "ServerDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Наибольший возможный номер порта (вряд ли изменится в ближайшие десятилетия).
#define IPPORT_MAX	65535


CServerDialog::CServerDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SERVER_DIALOG, pParent), m_server(*this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CServerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SPIN_PORT, portSpinButton);
	DDX_Control(pDX, IDC_EDIT_PORT, portEdit);
	DDX_Control(pDX, IDC_BUTTON_TOGGLE_SERVER, toggleServerButton);
	DDX_Control(pDX, IDC_LISTBOX_STRINGS, stringListBox);
	DDX_Control(pDX, IDC_LISTBOX_CLIENTS, clientsListBox);

	DDX_Text(pDX, IDC_EDIT_PORT, m_port);
	DDV_MinMaxUInt(pDX, m_port, 1, IPPORT_MAX);
}

BEGIN_MESSAGE_MAP(CServerDialog, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_TOGGLE_SERVER, &CServerDialog::OnBnClickedButtonToggleServer)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


BOOL CServerDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	portSpinButton.SetBuddy(CWnd::FromHandle(portEdit.m_hWnd));
	portSpinButton.SetRange32(1, IPPORT_MAX);
	portSpinButton.SetPos32(SLTP::DefaultPort);

	return TRUE;  // Возврат TRUE означает, что фокус не передан элементу управления.
}


void CServerDialog::OnBnClickedButtonToggleServer()
{
	if (m_server.IsRunning())
	{
		m_server.Stop();
		toggleServerButton.SetWindowTextW(_T("Запустить сервер"));
		SetConnectionControlsEnabled(true);
	}
	else
	{
		if (UpdateData())
		{
			m_server.Start(m_port);
			SetConnectionControlsEnabled(false);
			toggleServerButton.SetWindowTextW(_T("Остановить сервер"));
		}
	}
}

void CServerDialog::SetClients(std::list<CString> clients)
{
	clientsListBox.ResetContent();
	for (std::list<CString>::iterator client = clients.begin(); client != clients.end(); ++client)
	{
		clientsListBox.AddString(*client);
	}
}


void CServerDialog::SetStrings(std::map<SLTP::STRING_ID, CString> strings)
{ 
	stringListBox.ResetContent();
	for (std::map<SLTP::STRING_ID, CString>::iterator pair = strings.begin(), last = strings.end();
		 pair != last;
		 ++pair)
	{
		int index = stringListBox.AddString(pair->second);
		stringListBox.SetItemData(index, pair->first);
	}
}


void CServerDialog::ShowError(const CString& errorMessage)
{
	m_server.Stop();
	AfxMessageBox(errorMessage, MB_OK | MB_ICONERROR);
	toggleServerButton.SetWindowTextW(_T("Перезапустить сервер"));
	SetConnectionControlsEnabled(true);
}


void CServerDialog::SetConnectionControlsEnabled(const bool enabled)
{
	portEdit.EnableWindow(enabled);
	portSpinButton.EnableWindow(enabled);
}


void CServerDialog::OnOK()
{
	if (!m_server.IsRunning())
	{
		OnBnClickedButtonToggleServer();
	}
}


void CServerDialog::OnClose()
{
	CDialogEx::OnCancel();
}
