﻿#pragma once

#include <afxcmn.h>
#include <afxwin.h>

#include "resource.h"

#include "SltpServer.h"


// Главное окно приложения-сервера в форме диалога Windows.
// Содержит объект-сервер SLTP, для которого само является представлением.
class CServerDialog : public CDialogEx, public IServerView
{
public:
	CServerDialog(CWnd* pParent = NULL);


protected:
	// Реализует поддержку Dialog Data Exchange & Validation (DDX/DDV) ---
	// органичного способа передачи данных из элементов управления в программу
	// с проверкой вводимых пользователем значений.
	virtual void DoDataExchange(CDataExchange* pDX);


public: // Реализация интерфейса IServerView для поддержки представления сервера.

	// Вызывается сервером для установки списка клиентов (наполняет список).
	virtual void SetClients(std::list<CString> clients);

	// Вызывается сервером для установки списка строк (наполняет список).
	virtual void SetStrings(std::map<SLTP::STRING_ID, CString> strings);

	// Вызывается сервером при возникновении ошибок (показывает сообщение).
	virtual void ShowError(const CString& errorMessage);

private:
	// Сервер SLTP.
	CSltpServer m_server;

	// Порт для соединения с сервером.
	UINT m_port;

private:
	void SetConnectionControlsEnabled(const bool enabled);


protected: // Детали реализации, связанные с MFC.

	// Значок окна.
	HICON m_hIcon;

	// Вызывается при инициализации диалога. Настраивает элементы управления.
	virtual BOOL OnInitDialog();

	// Карта сообщений (используется MFC для реакции на события).
	DECLARE_MESSAGE_MAP()


private: // Элементы управления.
	CSpinButtonCtrl portSpinButton;
	CEdit portEdit;
	CButton toggleServerButton;
	CListBox stringListBox;
	CListBox clientsListBox;
	CEdit hostEdit;

private: // Обработчики событий.

	// Вызывается при нажатии на кнопку "Запустить/остановить сервер".
	afx_msg void OnBnClickedButtonToggleServer();


public: // Переопределенные методы диалога.

	// Вызывается при окончании диалога положительным результатом (кнопка "ОК" или ВВОД).
	// Переопределен, чтобы запускать сервер, если он не запущен, и окно не закрывать.
	virtual void OnOK();

	// Вызывается при окончании диалога отрицательным результатом (кнопка "Отмена" или Escape).
	// Переопределен, чтобы ничего не делать и окно не закрывать.
	virtual void OnCancel() { }

	// Вызывается при закрытии окна "крестиком" или из панели задач.
	// Перекрыт, чтобы игнорирование окончания работы с диалогом (OnCancel)
	// не мешало нормальному закрытию окна.
	afx_msg void OnClose();
};
