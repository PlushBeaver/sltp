﻿#include "stdafx.h"
#include "SltpServer.h" 

// Макрос для перебора клиентов. С++11 (VS2012 и выше) позволил бы обойтись без него.
#define FOR_EACH_CLIENT	\
	for (std::list<CSltpServerSocket*>::iterator client = m_clients.begin(), lastClient = m_clients.end();	\
	client != lastClient; \
	++client)


CSltpServer::CSltpServer(IServerView& view) : CAsyncSocket(), m_view(view)
{
	m_isRunning = false;
}

CSltpServer::~CSltpServer(void)
{
	Stop();
}

void CSltpServer::Start(const UINT port)
{
	m_clients.clear();
	if (!Create(port))
	{
		m_view.ShowError(_T("Не удалось создать сокет для приема входящих подключений!"));
	}
	if (!Listen())
	{
		m_view.ShowError(_T("Не удалось начать ожидание входящих подключений!"));
	}
	m_isRunning = true;
}

void CSltpServer::Stop()
{
	if (m_isRunning)
	{
		Close();
		Finalize();
	}
}

void CSltpServer::Finalize()
{
	m_isRunning = false;
	FOR_EACH_CLIENT
	{
		(*client)->Close();
	}
	m_clients.clear();
	UpdateViewClientList();
}

const bool CSltpServer::IsRunning() const 
{
	return m_isRunning;
}

void CSltpServer::OnAccept(int nErrorCode)
{
	CSltpServerSocket *client = new CSltpServerSocket(*this);
	if (Accept(*client))
	{
		m_clients.push_back(client);
		UpdateViewClientList();

		client->ClearList();

		std::map<SLTP::STRING_ID, CString>::iterator 
			pair = m_strings.begin(), lastPair = m_strings.end();
		while (pair != lastPair)
		{
			client->AcceptString(pair->second, pair->first);
			++pair;
		}
	}
	else
	{
		delete client;
		m_view.ShowError(_T("Не удалось подключить клиента!"));
	}
	CAsyncSocket::OnAccept(nErrorCode);
}


void CSltpServer::OnClose(int nErrorCode)
{
	Finalize();
	CAsyncSocket::OnClose(nErrorCode);
}

void CSltpServer::OnAddString(const CString& string)
{
	m_strings[m_lastStringID] = string;
	m_view.SetStrings(m_strings);
	FOR_EACH_CLIENT
	{
		(*client)->AcceptString(string, m_lastStringID);
	}
	m_lastStringID++;
}

void CSltpServer::OnRemoveString(const SLTP::STRING_ID stringID)
{
	m_strings.erase(stringID);
	m_view.SetStrings(m_strings);
	FOR_EACH_CLIENT
	{
		(*client)->RemoveString(stringID);
	}
}

void CSltpServer::OnClientDisconnected(SLTP::IServerSide& client)
{
	CSltpServerSocket* deceased = dynamic_cast<CSltpServerSocket*>(&client);
	if (deceased)
	{
		m_clients.remove(deceased);
	}
	UpdateViewClientList();
}

void CSltpServer::OnErrorOccurred(const CString& errorMessage)
{
	m_view.ShowError(errorMessage);
}

void CSltpServer::UpdateViewClientList()
{
	std::list<CString> clients;
	FOR_EACH_CLIENT
	{
		CString remoteName, clientCaption;
		UINT remotePort;
		(*client)->GetPeerName(remoteName, remotePort);
		clientCaption.Format(_T("%s:%u"), remoteName.GetString(), remotePort);
		clients.push_back(clientCaption);
	}
	m_view.SetClients(clients);
}

#undef FOR_EACH_CLIENT
