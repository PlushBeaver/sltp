﻿#pragma once
#include "afxsock.h"

#include <list>
#include <map>

#include "../Library/SLTP.h"
#include "SltpServerSocket.h"

// Интерфейс представления сервера SLTP.
class IServerView
{
public:
	// Устанавливает список клиентов.
	virtual void SetClients(std::list<CString> clients) = 0;

	// Устанавливает список строк с их идентификаторами.
	virtual void SetStrings(std::map<SLTP::STRING_ID, CString> strings) = 0;

	// Демонстрирует пользователю сообщение об ошибке.
	virtual void ShowError(const CString& errorMessage) = 0;
};


// Сервер SLTP. Синхронизирует список строк между клиентами,
// списком которых управляет, и позволяет извне управлять списком строк.
class CSltpServer : public CAsyncSocket, public SLTP::IServerSideEventListener
{
public:
	// Создает сервер SLTP с заданным представлением.
	CSltpServer(IServerView& view);
	
	// Корректно останавливает обработку запросов и удаляет объект сервера SLTP.
	virtual ~CSltpServer(void);


public:	// Переопределенные методы CAsyncSocket. 
	
	// Вызывается при возможности принять входящее подключение.
	virtual void OnAccept(int nErrorCode);

	// Вызывается при закрытии сокета сервера (ожидающего подключения) по любой причине.
	virtual void OnClose(int nErrorCode);

	 
public:	// Реализация интерфейса ISltpServerSideEventListener.
		// Один объект-сервер реагирует на события всех клиентов.

	// Вызывается по приходе от клиента строки для добавления в список.
	virtual void OnAddString(const CString& string);

	// Вызывается по приходе от клиента команды удалить строку.
	virtual void OnRemoveString(const SLTP::STRING_ID stringID);

	// Вызывается при отсоединении клиента (передается параметром).
	virtual void OnClientDisconnected(SLTP::IServerSide& side);

	// Вызывается при возникновении ошибок при работе с клиентами.
	virtual void OnErrorOccurred(const CString& errorMessage);


public:	// Методы управления сервером.

	// Запускает сервер на указанном порту.
	void Start(const UINT port);

	// Останавливает работу сервера.
	void Stop();

	// Возвращает признак, работает ли сервер.
	const bool IsRunning() const;


private: // Детали реализации.

	// Обновляет список клиентов у представления.
	void UpdateViewClientList();

	// Корректно завершает работу сервера при закрытии или остановке.
	void Finalize();

private:
	// Признак, что сервер работает.
	bool m_isRunning;

	// Представление.
	IServerView& m_view;
	
	// Список строк. поставленных в соответствие своим идентификаторам.
	std::map<SLTP::STRING_ID, CString> m_strings;

	// Последний выданный идентификатор (для генерации).
	SLTP::STRING_ID m_lastStringID;

	// Список клиентов.
	std::list<CSltpServerSocket*> m_clients;
};

