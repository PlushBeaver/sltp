﻿#include "stdafx.h"
#include "SltpServerSocket.h"


CSltpServerSocket::CSltpServerSocket(SLTP::IServerSideEventListener& eventListener)
	: m_eventListener(eventListener)
{
}


void CSltpServerSocket::OnClose(int nErrorCode)
{
	CSltpSocket::OnClose(nErrorCode);
	m_eventListener.OnClientDisconnected(*this);
}


CSltpSocket::ParsingState CSltpServerSocket::OnCommandParsed()
{
	switch (m_command)
	{
	case CommandCode_AddString:
		return ParsingState_StringLength;
	}
	return CSltpSocket::OnCommandParsed();
}


CSltpServerSocket::ParsingState CSltpServerSocket::OnStringDataParsed(const CString& string)
{
	switch (m_command)
	{
	case CommandCode_AddString:
		m_eventListener.OnAddString(string);
		return ParsingState_Command;
	}
	return ParsingState_Invalid;
}


void CSltpServerSocket::AcceptString(const CString& string, SLTP::STRING_ID stringID)
{
	SendByte(CommandCode_AcceptString);
	SendInteger(stringID);
	SendString(string);
	OnSend(0);
}


void CSltpServerSocket::ClearList()
{
	SendByte(CommandCode_ClearList);
	OnSend(0);
}


void CSltpServerSocket::RemoveString(const SLTP::STRING_ID stringID)
{
	SendByte(CommandCode_RemoveString);
	SendInteger(stringID);
	OnSend(0);
}
