﻿#pragma once

#include "../Library/SLTP.h"
#include "../Library/SltpSocket.h"

// Сокет серверной стороны SLTP.
class CSltpServerSocket : public CSltpSocket, public SLTP::IServerSide
{
public:
	// Создает сокет серверной части SLTP, оповещающий о событиях заданного слушателя.
	CSltpServerSocket(SLTP::IServerSideEventListener& eventListener);


public:	// Реализация интерфейса SLTP::IServerSide.

	// Отправляет клиенту утвержденную в списке строку и её идентификатор.
	virtual void AcceptString(const CString& string, SLTP::STRING_ID stringID);

	// Отправляет клиенту команду удалить строку с заданным идентификатором из списка.
	virtual void RemoveString(const SLTP::STRING_ID stringID);

	// Отправляет клиенту команду очистить список.
	virtual void ClearList();


protected:	// Переопределения методов сокета.

	// Вызывается после закрытия сокета, оповещает слушателя, что клиент отсоединен.
	virtual void OnClose(int nErrorCode);


protected:	// Переопределение и реализация методов поддержки SLTP.

	// Вызывается после разбора кода команды. Для сервера обрабатываются
	// запросы на добавление и удаление строки; о последнем оповещается слушатель событий.
	virtual ParsingState OnCommandParsed();

	// Вызывается после считывания данных строки. Для сервера это команда
	// запроса на добавление строки, о котором оповещается слушатель событий.
	virtual ParsingState OnStringDataParsed(const CString& string);

	// Возвращает интерфейс слушателя событий любой стороны протокола.
	virtual SLTP::IEventListener& GetEventListener() const { return m_eventListener; }


private:
	// Слушатель событий серверной части SLTP.
	SLTP::IServerSideEventListener& m_eventListener;
};

