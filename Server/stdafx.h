﻿#pragma once

// Исключает редко используемые компоненты из заголовков Windows.
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN          
#endif

// Задает версию используемого Windows Platform SDK.
#include "targetver.h"

// Способствует объявлению некоторы конструкторов CString как explicit.
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS

// Отключает функцию скрытия некоторых общих и часто пропускаемых предупреждений MFC.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>			// Основные и стандартные элементы управления MFC.
#include <afxsock.h>		// Поддержка сетевой подсистемы.
#include <afxcontrolbars.h>	// Поддержка стилей оформления и современных элементов управления.
#include <afxdialogex.h>	// Усовершенствованный класс диалога CDialogEx.

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Поддержка MFC для типовых элементов управления Internet Explorer 4.
#endif

#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Поддержка типовых элементов управления Windows (Common Controls).
#endif

// Включение в исполняемый файл манифеста для Common Controls.
#ifdef _UNICODE
#	if defined _M_IX86
#		pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#	elif defined _M_X64
#		pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#	else
#		pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#	endif
#endif